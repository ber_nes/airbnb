# Akka Airbnb Project

Airbnb app taken from kaggle.com. Link: https://www.kaggle.com/datasets/samyukthamurali/airbnb-ratings-dataset?select=airbnb-reviews.csv

The dataset contains data from listings, hosts and reviews of all the world.
### Entities

The separate entities are:

#### Listing

The dataset has more than one million of records and 268 MB
#### Host

The listing dataset has information for the host therefore that has one million of host

#### Reviews

The reviews dataset has 3.22 GB of data

### Instructions for run the app

For run the app:

This command run the cassandra instance

    sudo docker-compose up

After, launch the app with sbt

    sbt run

### Endpoints

The guide for use the endpoints is presented in the json file **airbnb.postman_collection.json** that can be load to a postman collection

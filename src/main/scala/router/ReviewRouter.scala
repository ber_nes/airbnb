package router

import java.time.LocalDate
import java.util.UUID
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}

import akka.Done
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.ClusterSharding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import entity.{ListingEntity, ReviewEntity}
import akka.http.scaladsl.server.Directives._
import akka.util.Timeout
import spray.json.DefaultJsonProtocol._
import spray.json._

class ReviewRouter(system: ActorSystem[_]) {

  private val sharding = ClusterSharding(system)

  implicit val timeout: Timeout = Timeout(5.seconds)

  case class ReviewInput(listingId: String, reviewerId: String, reviewerName: String, comment: String)
  case class Comment(comment: String)

  implicit val reviewInputFormat: RootJsonFormat[ReviewInput] = jsonFormat4(ReviewInput)
  implicit val dateFormat: JsonFormat[LocalDate] = new JsonFormat[LocalDate] {
    override def write(obj: LocalDate): JsValue = JsString(obj.toString)

    override def read(json: JsValue): LocalDate = json match {
      case JsString(s) => Try(LocalDate.parse(s)) match {
        case Success(result) => result
        case Failure(exception) =>
          deserializationError(s"could not parse $s as LocalDate", exception)
      }
      case _ => deserializationError(s"Something went wrong with the deserialization")
    }
  }
  implicit val comment: RootJsonFormat[Comment] = jsonFormat1(Comment)
  implicit val reviewFormat: RootJsonFormat[ReviewEntity.Review] = jsonFormat6(ReviewEntity.Review)
  implicit val failureResponseFormat: RootJsonFormat[FailureResponse] = jsonFormat1(FailureResponse)

  case class FailureResponse(reason: String)

  private def createReview(reviewInput: ReviewInput): Future[ReviewEntity.Response] = {
    val ref = sharding.entityRefFor(ListingEntity.TypeKey, reviewInput.listingId)
    val listing = ref.ask(replyTo => ListingEntity.GetListing(reviewInput.listingId, replyTo))
    implicit val actorSystem: ExecutionContextExecutor = system.executionContext
    listing.flatMap {
      case ListingEntity.GetListingResponse(Some(_)) =>
        val id = UUID.randomUUID().toString
        val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
        ref.ask { replyTo =>
          ReviewEntity.AddReview(
            id,
            reviewInput.listingId,
            reviewInput.reviewerId,
            reviewInput.reviewerName,
            reviewInput.comment,
            replyTo
          )
        }
      case ListingEntity.GetListingResponse(None) =>
        Future(ReviewEntity.ReviewAddedResponse(Failure(new RuntimeException("Listing can not be found"))))
      case ListingEntity.AlreadyDeletedResponse =>
        Future(ReviewEntity.ReviewAddedResponse(Failure(new RuntimeException("Listing can not be found"))))
      case _ => Future(ReviewEntity.ReviewAddedResponse(Failure(new RuntimeException("Something went wrong"))))
    }
  }

  private def updateReview(id: String, comment: Comment): Future[ReviewEntity.Response] = {
    val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
    ref.ask(replyTo => ReviewEntity.UpdateReview(comment.comment, replyTo))
  }

  def getReview(id: String): Future[ReviewEntity.Response] = {
    val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
    ref.ask(replyTo => ReviewEntity.GetReview(id, replyTo))
  }

  val routesReview: Route = pathPrefix("review") {
    concat(
      path(Segment) { id =>
        get {
          onSuccess(getReview(id)) {
            case ReviewEntity.GetReviewResponse(Some(review)) =>
              complete(StatusCodes.OK, review)
            case ReviewEntity.GetReviewResponse(None) =>
              complete(StatusCodes.NotFound)
            case ReviewEntity.AlreadyDeletedResponse =>
              complete(StatusCodes.NotFound)
            case _ => complete(StatusCodes.InternalServerError)
          }
        } ~
        put {
          entity(as[Comment]) { comment =>
            onSuccess(updateReview(id, comment)) {
              case ReviewEntity.ReviewUpdatedResponse(Success(Done)) =>
                complete(StatusCodes.OK)
              case ReviewEntity.ReviewUpdatedResponse(Failure(exception)) =>
                complete(StatusCodes.BadRequest, FailureResponse(s"${exception.getMessage}"))
              case ReviewEntity.AlreadyDeletedResponse =>
                complete(StatusCodes.NotFound)
              case _ => complete(StatusCodes.InternalServerError)
            }
          }
        } ~
        delete {
          val ref = sharding.entityRefFor(ReviewEntity.TypeKey, id)
          ref ! ReviewEntity.RemoveReview
          complete(StatusCodes.NoContent)
        }
      },
      pathEndOrSingleSlash {
        post {
          entity(as[ReviewInput]) { review =>
            onSuccess(createReview(review)) {
              case ReviewEntity.ReviewAddedResponse(Success(review)) =>
                complete(StatusCodes.Created, review)
              case ReviewEntity.ReviewAddedResponse(Failure(exception)) =>
                complete(StatusCodes.BadRequest, FailureResponse(s"${exception.getMessage}"))
              case _ => complete(StatusCodes.InternalServerError)
            }
          }
        }
      }
    )
  }

}
